INTRODUCTION
------------

This module integrates the AOS JS with AnimateCSS module
  - https://michalsnik.github.io/aos/

AOS.js is a library of ready-to-use, cross-browser animations
for use in your web projects. Great for emphasis, home pages, sliders,
and attention-guiding hints.


FEATURES
--------

'AOS.js' library is:

  - Cross-browser animations

  - Usage with Javascript

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

'AnimateCSS' module:
  - https://www.drupal.org/project/animatecss

'AOS JS' module:
  - https://www.drupal.org/project/aosjs

INSTALLATION
------------

1. Download 'animatecss_aos' module with dependency modules
   - https://www.drupal.org/project/animatecss_aos
   - https://www.drupal.org/project/animatecss
   - https://www.drupal.org/project/aosjs

2. Extract and place all in the root of contributed modules directory i.e.
   /modules/contrib/ or /modules/

3. Now, enable 'AnimateCSS On Scroll' module


USAGE
-----

It's very simple, you only need to enable AOS option in the
Add animation form.


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://animate.style/
