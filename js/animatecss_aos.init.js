/**
 * @file
 * Contains definition of the behaviour AnimateCSS AOS.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  // Set AnimateCSS compat version for change classes.
  const compat = drupalSettings.animateCssAOS.compat;
  const library = drupalSettings.aosjs.library;

  Drupal.behaviors.animateCssAosInit = {
    attach: function (context, settings) {

      // Set AnimateCSS and AOS records.
      const elements = drupalSettings.animateCssAOS.elements;
      let isItemInit = false;

      // Set AnimateCSS and AOS settings for each record.
      $.each(elements, function (index, element) {
        let options = {
          selector: element.selector,
          library: element.library ? element.library : library,
          animation: element.animation,
          repeat: element.repeat,
          offset: element.offset ?? element.aos.offset,
          delay: element.delay,
          duration: element.duration,
          easing: element.easing ?? element.aos.easing,
          once: element.once ?? element.aos.once,
          mirror: element.mirror ?? element.aos.mirror,
          anchorPlacement: element.anchorPlacement ?? element.aos.anchorPlacement,
          clean: element.clean ?? false,
          display: element.display ?? false,
        };

        // Prepares AOS if selector is available.
        if (once('animatecss-aos', options.selector).length) {
          new Drupal.animateCssAosPrepare(options);
          // If there is even a selector then
          // the init value must be set to true.
          isItemInit = true;
        }
      });

      // Check is there item to init first.
      if ( isItemInit ) {
        // Set additional advanced setting for version 3.
        let additional = {
          initClassName: 'aos-init',
          animatedClassName: 'aos-animate',
          useClassNames: true,
        };

        // Initial AOS now.
        AOS.init( additional );
      }

    }
  };

  Drupal.animateCssAosPrepare = function (options) {
    // Remove previous animation classes if 'clean' is true.
    if (options.clean) {
      $(options.selector).removeClass(function (index, className) {
        return (className.match(/(^|\s)animate__\S+/g) || []).join(' ');
      });
    }

    let prefix = compat ? '' : 'animate__'
      , animation = options.library === 'animate' ? 'animate__animated ' + prefix + options.animation : options.animation;

    if (options.library === 'animate') {
      // Add Animate.css delay properties.
      if ( options.delay ) {
        $( options.selector ).css({
          '-webkit-animation-delay': options.delay + 'ms',
          '-moz-animation-delay': options.delay + 'ms',
          '-ms-animation-delay': options.delay + 'ms',
          '-o-animation-delay': options.delay + 'ms',
          'animation-delay': options.delay + 'ms',
          '--animate-delay': options.delay + 'ms',
        });
      }

      // Add Animate.css duration properties.
      if ( options.duration ) {
        $( options.selector ).css({
          '-webkit-animation-duration': options.duration + 'ms',
          '-moz-animation-duration': options.duration + 'ms',
          '-ms-animation-duration': options.duration + 'ms',
          '-o-animation-duration': options.duration + 'ms',
          'animation-duration': options.duration + 'ms',
          '--animate-duration': options.duration + 'ms',
        });
      }

      // Add Animate.css repeat class.
      if ( options.repeat && options.repeat != 'repeat-1' ) {
        $(options.selector).addClass(prefix + options.repeat);
      }

      // Check and change display property if required.
      if (options.display) {
        var displayValue = $(options.selector).css('display');
        if (displayValue === 'inline') {
          $(options.selector).css('display', 'inline-block');
        }
      }
    }

    // Build AOS.js from animateCSS form settings.
    $( options.selector ).attr( "data-aos", animation );
    $( options.selector ).attr( "data-aos-library", options.library );
    $( options.selector ).attr( "data-aos-offset", options.offset );
    $( options.selector ).attr( "data-aos-delay", options.delay );
    $( options.selector ).attr( "data-aos-duration", options.duration );
    $( options.selector ).attr( "data-aos-easing", options.easing );
    $( options.selector ).attr( "data-aos-anchor-placement", options.anchorPlacement );
    $( options.selector ).attr( "data-aos-once", options.once === 1);
    $( options.selector ).attr( "data-aos-mirror", options.mirror === 1);

  };

})(jQuery, Drupal, drupalSettings, once);
